@extends('layouts.master')
@section('styles')
    <link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection


@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-life-ring"></i> Nueva comision </h3>
        </div>
    </div>

    <div class="clearfix"></div>
<button type="button" class="btn btn-secondary" id="hola">Secondary</button>

    <form id="form" method="POST" action="{{ url('viatico') }}" accept-charset="UTF-8">
        @csrf
        @yield('body')
    </form>




<!-- Modal -->
<div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 15%;">



  <form id ="myFormGanador" method="post" action={{ url('viatico_confirmacion')}} >
        @csrf
        <input name="_method" type="hidden" value="PATCH">

    <div class="modal-content">

    <div class="modal-header">
        <h2 id="modal_titulo" style="font-size:30px; color: black; text-align: center;"> <b><u>  DATOS DE LA COMISION </b></u>  </h2>
      </div>

    <br>
      <div class="modal-body" id = "modal_ganador_texto" style="font-size:30px; color: black;">
      
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnEnviar" name="btnGanador" class="btn btn-outline-secondary"> Aceptar </button>
    
        
      </div>
    </div>
    </form>
  </div>
</div>

@endsection

@section('js')
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});


$("#personal").change(function() {
    var str_array = $('#personal').val();

    var array = $('#personal option:not(:selected)');

    var $dropdown = $("#chofer");
    $dropdown.empty();
    $.each(array, function() {
        $dropdown.append($("<option />").val(this.value).text(this.text));
    });

    });


$('#lbl_monto_reintegro').toggle();
$('#reintegro').toggle();    

$("#check_reintegro").change(function(){
    $('#lbl_monto_reintegro').toggle();
    $('#reintegro').toggle();
});

$('#myModalGanador').on('show.bs.modal', function(e) {

    var empleados = $("#personal").val() + ',' +  ($("#chofer").val());
    var texto_html = '';
    var coso = '';

    $("#modal_ganador_texto").html('<b> Resolucion: </b>' + $("#resolucion").val()    
    + '<br><b>Act/exp : </b>' +  $("#exp1").val() + '-' + $("#exp2").val() + '-' + $("#exp3").val() + '-' + $("#exp4").val() + '-' + $("#exp5").val()
    + '<br><b>Localidades :</b>' + $("#localidades option:selected").text()
    + '<br><b>Dias : </b>' +  $("#dias").val()
    + '<br><b>Combustible : $</b>' +  $("#combustible").val() 
    + ' (' + $("#chofer option:selected").text() + ')' );
    if($("#externo").val() != 0) {$("#modal_ganador_texto").append('<br><b>Fuera de la provincia: $</b>' +  $("#gastos").val());}
    if($("#gastos").val() != 0) {$("#modal_ganador_texto").append('<br><b>Otros gastos : $</b>' +  $("#gastos").val());}
    if($("#reintegro").val() != 0) {$("#modal_ganador_texto").append('<br><b>Reintegro : $</b>' +  $("#reintegro").val());}

    $("#modal_ganador_texto").append('<br><b>Personal: </b> <br>');
    $.ajax({                        
            type: "GET",                 
            url: "{{ URL::to('viatico_confirmacion') }}",                     
            data: {'empleados' : empleados}, 
            success: function(array)             
            {
                array.forEach(function(element) {
                $("#modal_ganador_texto").append(' - ' + element.empleados + '<BR>');
                });
            }
        });
    });

$( "#btnEnviar").click(function() {
//    alert('hola');
  
  var str = "{{ URL::to('viatico') }}";
//  var res = str.replace("ID", id);

   $.ajax({                        
          type: "POST",                 
          url: str,                     
          data: $("#form").serialize(), 
          success: function(result)             
          {
            $('#mensaje_alerta').toggle(1500);

            setTimeout(function ()
            {
              $('#myModalGanador').modal('toggle'); 
//              table.ajax.reload( null, false );    
            }, 1500);
 


          }
       });

});

</script>

@endsection