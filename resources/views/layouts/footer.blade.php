<!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 IPDUV Sistemas
                    </div>
                    <div class="col-sm-6 text-right">
                        Desarrollado por <a href="#">Federico Barraza</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->