<?php

use Illuminate\Database\Seeder;
use App\Tipo_Resolucion;

class TipoResolucionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'ADJUDICACIÓN DE OBRA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'AFECTAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'ANTICIPO FUENTES DE RECURSOS';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'ANTICIPO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'APROBAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'APROPIACIÓN AL EJERCICIO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'AYUDA ECONÓMICA AL BENEFICIARIO Y SU GRUPO FAMILIAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'BONIFICACIÓN POR TITULO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'BÚSQUEDA DE ACTUACIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'CAJA CHICA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'CANCELACIÓN DE HIPOTECA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'CERTIFICADO DE DESEMBOLSO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'COMISIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'CONTRATAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DAR DE BAJA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DEJAR SIN EFECTO RESOLUCIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DEJAR SIN EFECTO ADSCRIPCIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DEJAR SIN EFECTO DONACIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DEJAR SIN EFECTO LICENCIA EXTRAORDINARIA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'DEPOSITAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'HABILITACIÓN DE CAJA CHICA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'LICENCIA EXTRAORDINARIA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'LLAMADO A CONCURSO DE PRECIOS';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'LLAMADO A LICITACIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'MEJORAR RENOVAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'NUEVO MONTO DE COMBUSTIBLE';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'ORDEN DE PAGO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PAGO A LA FIRMA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PAGO CERTIFICADO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'REALIZAR TRANSFERENCIA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'REGIONALIZACIÓN';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'RENDICIÓN DE VIÁTICOS PRESENTADA';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PAGO A LA FIRMA MEJORAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PRESTAMO DE VEHICULO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'REINTEGRO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'REDETERMINACION DE PRECIO';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PAGAR A ';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        $entidad = new Tipo_Resolucion();
        $entidad->nombre = 'PAGO A LA FIRMA MEJORAR';
        if(Tipo_Resolucion::where('nombre', $entidad->nombre)->count() == 0 ){
            $entidad->save();
        }

        
        

    }
}
