@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">Cargar deuda</div>
        <div class="panel-body">

          @include('partials.flash-message')

          <form method="POST" action={{ url('import_deuda')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
              
              <label class="col-md-4 control-label">Seleccione el archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="file" >
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Cargar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection