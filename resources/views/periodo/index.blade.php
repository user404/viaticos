@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span> 
        </button>
         <h4 id="titulo_modal">Cerrar periodo actual</h4> 
      </div>
      <div class="modal-body">
        <form method="POST" action="periodo/1">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <input type="hidden" id="periodo_id" name="periodo_id" value="1">
         <input type="hidden" id="valor_periodo" name="valor_periodo" value="">

            <div class="form-group">
                <p id="periodo_text"> </p>
            </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"> Aceptar</button>

        </form>
      
      </div>
    </div>
  </div>
</div>


@endsection

@section('javascripts')
<script>
  
  $('#myModal').on('show.bs.modal', function(e) { 


//    var str = "{{ URL::to('deuda_periodo/ID') }}";
//    var res = str.replace("ID", id_jurisdiccion);

    $.ajax({
        type: 'get',
        url: 'ultimo_periodo_ajax', 
        success: function (data) {
          $('#valor_periodo').val(data.periodo);
          $('#titulo_modal').html( 'Cerrar periodo actual (' + data.periodo1.substr(5, 2) + '/' + data.periodo1.substr(0, 4) + ')' );
          $('#periodo_text').html('Seguro que desea cerrar el periodo actual y abrir el: ' + data.periodo.substr(5, 2) + '/' + data.periodo.substr(0, 4) + '?' ) 

        }
    });

  })

</script>

@endsection