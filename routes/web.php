<?php

use App\Comision;
use App\Resolucion;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('probando', 'ComisionController@probando', ['middleware' => ['auth']]);

Route::get('listar_tipos_de_resoluciones', 'AjaxController@listarTiposDeResoluciones', ['middleware' => ['auth']]);

//Route::resource('tipo_resolucion', 'TipoResolucionController', ['middleware' => ['auth']]);

//Route::post('tipo_resolucion', 'TipoResolucionController@store', ['middleware' => ['auth']]);

Route::resource('tipo_resolucion', 'TipoResolucionController', ['middleware' => ['auth']]);




Route::get('gastos', 'AjaxController@gastos', ['middleware' => ['auth']]);
Route::post('store_gastos', 'ComisionController@storeGastos', ['middleware' => ['auth']]);





Route::get('otras_resoluciones', function () { return view('resoluciones/index ');});

Route::get('agente_comision', 'AjaxController@agenteComision', ['middleware' => ['auth']]);

Route::get('detalle_resoluciones', 'AjaxController@resolucionesAjax', ['middleware' => ['auth']]);

Route::get('/home', 'HomeController@index')->name('home');
// Auth::routes(['register' => false]);
Auth::routes();
Route::get('/', function () { return view('auth/login');});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('comision', 'ComisionController', ['middleware' => ['auth']]);
Route::get('index_ajax', 'AjaxController@indexAjax', ['middleware' => ['auth']]);
Route::get('detalle_ajax', 'AjaxController@detalleViaticosAjax', ['middleware' => ['auth']]);


Route::get('comision_confirmacion', 'AjaxController@comisionConfirmacion', ['middleware' => ['auth']]);

//Route::get('/cargar_safycs', function () { return view('import/safyc');}, ['middleware' => ['auth']]);

/*
Route::get('/cargar_safycs', function () { return view('import/safyc');}, ['middleware' => ['auth']]);
Route::post('import_safyc', 'ImportController@importSafyc', ['middleware' => ['auth']]);
Route::get('/cargar_deudas', function () { return view('import/deuda ');});
Route::post('import_deuda', 'ImportController@importDeuda', ['middleware' => ['auth']]);
*/


Route::resource('agente', 'AgenteController', ['middleware' => ['auth']]);