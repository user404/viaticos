@extends('layouts.master')
@section('styles')

<!-- DATEPICKER CSS -->
    <link href="{{ url('datepicker_gijgo/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datepicker_gijgo/gijgo.min.css') }}" rel="stylesheet">
<!-- DATATABLE CSS -->    
    <link href="{{ url('datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-usd"></i> Gastos </h3>
        </div>
    </div>

    <div class="clearfix"></div>
    @include('partials.flash-message')
  <br>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="card-header">
          Lista de agentes y gastos<strong></strong>
      </div>

      <div class="card-body card-block"> 

      <a href="{{ url('comision/create') }}"><button class='btn btn-outline-secondary'><span class='fa fa-arrow-left'></span> Cargar otra resolucion</button></a>

  <br><br>

    <table id="datatable-detalle" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">    
         <thead>
         <tr>
            <th>Nombre</th>
            <th>Gasto</th>
            <th>Agregar</th>
         </tr>
         </thead>
         <tbody>
            @foreach($comision_gastos as $l)
            <tr>
               <td>{{ $l->nombre }}</td>
               <td>
               {{ $l->pivot->gastos }}</td>
               <td>
               @if (!$l->gastos )
               <button type='button' id='btnModal' class='btn btn-outline-secondary' data-toggle='modal' data-target='#modal' data-agente-nombre = '{{ $l->nombre }}' data-agente-id =  {{ $l->pivot->agente_id }} data-comision-id = {{ $l->pivot->comision_id }}><span class='fa fa-usd'></span></button>
               @endif
               </td>
            </tr>
            @endforeach
         </tbody>
    </table>
    {{ $comision_gastos->links() }}

  <br>     

             
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('assets/images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog" role="document"  style="opacity:0.9 !important; top: 15%;">
    <div class="modal-content" >
    <div class="modal-header">
      <div class="col text-center">
          <h4 id="modal_titulo"> <b><u> Agregar Gasto </b></u>  </h4>
      </div>
    </div>

    <div class="modal-body">
    <form id = "formSorteo" method="POST" action={{ url('store_gastos')}} accept-charset="UTF-8" enctype="multipart/form-data">            
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
    <input type="hidden" name="comision_id" id="comision_id"> 
    <input type="hidden" name="agente_id" id ="agente_id"> 
        <div class="col-md-12">
          <div class = "row">
              <div class="col-md-2"> 
                <label> Gasto($): </label>
              </div>
              <div class="col-md-4"> 
                <input type="text" class="form-control" id= "gastos" name="gastos" required>
              </div>
          </div>
        </div>
           
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="btnEnviar" name="btnGanador" class="btn btn-outline-secondary"> Aceptar </button>      
      </div>
     </form> 
    </div>
  </div>
</div>

@endsection

@section('js')    
<!-- DATATABLE JS -->
<script src="{{ url('datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- DATEPICKER JS -->
<script src="{{ url('datepicker_gijgo/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/messages.es-es.js') }}" type="text/javascript"></script>



<script>  
  $(document).ready(function() { 
  });

$('#modal').on('show.bs.modal', function(e) {
  
    var $modal = $(this);
    var button = $(e.relatedTarget);
    
    var fecha_desde = $("#fecha_desde").val();
    var fecha_hasta = ($("#fecha_hasta").val());

    var agente_nombre = button.data('agente-nombre');
    var agente_id = button.data('agente-id');
    var comision_id = button.data('comision-id');

    $('#agente_id').val(agente_id);
    $('#comision_id').val(comision_id);
});
</script>

@endsection