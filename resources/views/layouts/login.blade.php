<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>IPDUV - Sistema de comisiones y viaticos</title>

    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ url('assets/css/normalize.min.css') }}">

    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <link rel="shortcut icon" href= "{{ url('assets/images/favicon.ico') }}" rel="stylesheet">
</head>
<body class="bg-dark" style=" background-repeat: no-repeat; background-image: url({{ url('assets/images/fondo.jpg') }}) ; background-size: cover; ">
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                            
                            <img class="align-content" src="{{ url('assets/images/logo.png') }}"  alt="">

                    </a>
                </div>
                <div class="login-form">
                    @yield('content')
                </div>            
            </div>            
        </div>
    </div>
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>
