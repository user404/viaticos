@extends('layouts.login')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div>
                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder='Usuario/E-mail' required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <div>
                <input id="password" type="password" class="form-control" name="password" placeholder="Clave" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-md-12">
        
            <div class=" form-group checkbox float-right" >
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar
                </label>
            </div>
            
            <div class="form-group" >
                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30"> Ingresar </button>
            </div>
        </div>

        <hr style="color: #0056b2;" />

        <h4> DEPARTAMENTO DE SISTEMAS</h4>
        <p class="float-right">©2019 Prog. Barraza, Federico</p>
    </form>

@endsection
