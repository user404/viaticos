@extends('layouts.master')
@section('styles')


    
    <link href="{{ url('select2/select2.min.css') }}" rel="stylesheet">
    
    <link href="{{ url('select2/estilo/dist/select2-bootstrap4.css') }}" rel="stylesheet">


    <!-- DATEPICKER CSS -->
    <link href="{{ url('datepicker_gijgo/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datepicker_gijgo/gijgo.min.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-life-ring"></i> Nueva Resolucion </h3>
        </div>
        @include('/partials/flash-message')
    </div>

    <div class="clearfix"></div>

<form id="form" method="POST" action="{{ url('viatico') }}" accept-charset="UTF-8">

    @csrf
    <br>

<div class = "row">
  <div class="col-md-6">
  <div class="card">

  <div class="card-header">
      Datos <strong>Comision</strong>
  </div>

      <div class="card-body card-block">
        

      <div class="form-row align-items-center">

        <div class="col-md-2"> 
          <label> Resolucion: </label>
        </div>
        <div class="col-md-3"> 
            <input type="number" class="form-control" id= "resolucion" name="resolucion" value="" required="">
        </div>  

        <div class="col-md-1"></div>  

    
        
        <div class="col-md-2">
            <label> Fecha: </label>
        </div>
        <div class="col-md-3"> 
        <input id="fecha" name="fecha"/>

        </div>
      </div>

      <br>

      <div class="form-row align-items-center">

          <div class="col-md-2">
            <label> Act/Exp: </label>
          </div>
          <div class="col-auto">
              <select class="form-control" id="exp1" name="exp1">
                  <option>E</option>
              </select>
          </div>

          <div class="col-auto" style="width:70px">
              <input type="number" class="form-control" id= "exp2" name="exp2" value="" required="" placeholder="10">
          </div>

          <div class="col-auto" style="width:90px">
              <input type="number" class="form-control" id= "exp3" name="exp3" required="" placeholder="Año" value="">
          </div>

          <div class="col-auto" style="width:130px">
              <input type="Number" class="form-control" id= "exp4" name="exp4" required="" placeholder="Numero" value="">
          </div>

          <div class="col-auto">
              <select class="form-control" id="exp5" name="exp5">
                  <option>A</option>
                  <option>E</option>
              </select>
          </div>
          
            <div class="col-md-3">
                <label> Fuera de la provincia? : </label>
            </div>
            <div class="col-auto">
                <input type="checkbox" id="externo" name="externo" value="1"/>
            </div>          
          
        </div>
        <br>
      
      <div class="form-row align-items-center">
        <div class="col-md-2"> 
          <label id="destino"> Localidades : </label>
        </div>
        <div class="col-md-5"> 
            <div id='div_localidades'>
                <select class="js-example-basic-multiple form-control" id= "localidades" name="localidades[]" multiple="multiple">
                    @foreach($localidades as $l)
                        <option value="{{$l->id}}">{{$l->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div id='div_provincias'>
                <select class="js-example-basic-single" id="provincias" name="provincias" style="width: 100%">
                <option disabled selected value> </option>
                    @foreach($provincias as $l)
                        <option value="{{$l->id}}">{{$l->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-1"> 
          <label> Dias: </label>
        </div>
        <div class="col-md-2"> 
            <input type="number" class="form-control" id= "dias" name="dias" required="" value= "1">
        </div>

      </div>
    

  </div>
    </div>
  </div>

<!-- FIN DE COLUMNA DE 6 -->

  <div class="col-md-6">
  <div class="card">
<div class="card-header">
    Gastos <strong>Comision</strong>
</div>

    <div class="card-body card-block">
      

    <div class="form-row align-items-center">
      <div class="col-md-2"> 
        <label> Combustible ($): </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "combustible" name="combustible" required="" value="0">
      </div>  


    </div>

    <br>

    <div class="form-row align-items-center">
      <div class="col-md-2"> 
        <label> Otros gastos ($): </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "gastos" name="gastos" required="" value="0">
      </div>
    </div>

    <br>

    <div class="form-row align-items-center">
    
        <div class="col-md-2">
            <label> Es reintegro?: </label>
        </div>

        <div class="col-auto" style="width:120px">
            <input type="checkbox" id="check_reintegro" name="check_reintegro" value="0"/>
        </div>


      <div class="col-md-2" id="lbl_monto_reintegro"> 
        <label> Monto($): </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "reintegro" name="reintegro" value="0" required="">
      </div> 
    </div>

</div>
</div>
</div>


</div>





<div class="card">
<div class="card-header">
    Datos <strong>Empleados</strong>
</div>
<div class="card-body card-block">

<div class="form-row align-items-center" >
        <div class="col-md-2"> 
          <label> Personas: </label>
        </div>
        <div class="col-md-4">
        <select class="js-example-basic-multiple form-control" id= "personal" name="personal[]" multiple="multiple">
            @foreach($personal as $l)
                <option value="{{$l->id}}">{{$l->nombre}}</option>
            @endforeach
        </select>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-1"> 
          <label> Chofer: </label>
        </div>


        <div class="col-md-3"> 
            <select class="js-example-basic-single form-control" id= "chofer" name="chofer">
            <option disabled selected value> </option>
            @foreach($personal as $l)
                <option value="{{$l->id}}">{{$l->nombre}}</option>
            @endforeach
        </select>
        </div>
      </div>
              <br><br>

        <button type="button" class="btn btn-outline-secondary float-right" data-toggle="modal"  data-target="#myModalGanador">Aceptar</button>
    </div>
    
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-user"></i> Login
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 15%;">
    <div class="modal-content">
    <div class="modal-header">
        <div class="col text-center">
            <h2 id="modal_titulo"> <b><u> DATOS DE LA COMISION </b></u>  </h2>
        </div>
      </div>
      <div class="modal-body" id = "modal_ganador_texto" style="font-size:30px; color: black;"></div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="btnEnviar1" name="btnGanador" class="btn btn-outline-secondary"> Aceptar </button>      
      </div>
    </div>
  </div>
</div>

</form>






@endsection

@section('js')
    

<script src="{{ url('select2/select2.min.js') }}" type="text/javascript"></script>

<script src="{{ url('datepicker_gijgo/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/messages.es-es.js') }}" type="text/javascript"></script>

<script>
$(document).ready(function() {

    var fecha = new Date();
    $('#fecha').datepicker({ locale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4', value : dateEs(fecha) });
    $('.js-example-basic-multiple').select2({ theme: 'bootstrap4', });
    $('.js-example-basic-single').select2({ theme: 'bootstrap4', });
});

$("#personal").change(function() {
    var str_array = $('#personal').val();
    var array = $('#personal option:not(:selected)');
    var $dropdown = $("#chofer");
    $dropdown.empty();
    $.each(array, function() {
        $dropdown.append($("<option />").val(this.value).text(this.text));
    });

    });

$('#lbl_monto_reintegro').toggle();
$('#reintegro').toggle();    
$('#div_provincias').toggle();    


$("#externo").change(function(){

    if($("#externo").is(":checked")){    
        $('#destino').text('Provincia: ');    
    } else {
        $('#destino').text('Localidades: ')
    }
    
    $('#div_localidades').toggle();    
    $('#div_provincias').toggle();    
});

$("#check_reintegro").change(function(){
    $('#lbl_monto_reintegro').toggle();
    $('#reintegro').toggle();
});


$("#externo").change(function(){
/*    $.ajax({                        
            type: "GET",                 
            url: "{{ URL::to('localidades_provincias') }}",                     
            data: {'empleados' : empleados}, 
            success: function(array)             
            {
                array.forEach(function(element) {
                $("#modal_ganador_texto").append(' - ' + element.empleados + '<BR>');
                });
            }
        });
        */
        console.log($("#externo").val());
});


$('#myModalGanador').on('show.bs.modal', function(e) {

    var empleados = $("#personal").val() + ',' +  ($("#chofer").val());
    var texto_html = '';
    var coso = '';

    $("#modal_ganador_texto").html('<b> Fecha: </b>' + $("#fecha").val()
    + '<br><b> Resolucion: </b>' + $("#resolucion").val()    
    + '<br><b>Act/exp : </b>' +  $("#exp1").val() + '-' + $("#exp2").val() + '-' + $("#exp3").val() + '-' + $("#exp4").val() + '-' + $("#exp5").val()
    + '<br><b>Localidades :</b>' + $("#localidades option:selected").text()
    + '<br><b>Dias : </b>' +  $("#dias").val()
    + '<br><b>Combustible : $</b>' +  $("#combustible").val() 
    + ' (' + $("#chofer option:selected").text() + ')' );
    if($("#externo").is(":checked")) {$("#modal_ganador_texto").append('<br><b>Fuera de la provincia: $</b>' +  $("#gastos").val());}
    if($("#gastos").is(":checked")) {$("#modal_ganador_texto").append('<br><b>Otros gastos : $</b>' +  $("#gastos").val());}
    if($("#reintegro").is(":checked")) {$("#modal_ganador_texto").append('<br><b>Reintegro : $</b>' +  $("#reintegro").val());}

    $("#modal_ganador_texto").append('<br><b>Personal: </b> <br>');
    $.ajax({                        
            type: "GET",                 
            url: "{{ URL::to('viatico_confirmacion') }}",                     
            data: {'empleados' : empleados}, 
            success: function(array)             
            {
                array.forEach(function(element) {
                $("#modal_ganador_texto").append(' - ' + element.empleados + '<BR>');
                });
            }
        });
    });

function dateEs(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}
$('#provincias').css('display', 'none');
</script>

@endsection