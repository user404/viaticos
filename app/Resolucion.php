<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Resolucion extends Model
{
    protected $table = 'resoluciones';

    public function comisiones()
    {
        return $this->hasMany('App\Comision');
    }

    public function tipo_resolucion()
    {
        return $this->belongsTo('App\Tipo_Resolucion');
    }

    public function resolucion_detalle()
    {
        return $this->hasMany('App\Resolucion_detalle');
    }

}
