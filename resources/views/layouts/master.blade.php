<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema de comisiones y viaticos</title>
    <meta name="description" content="IPDUV">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
    <!-- HREF DE FONTAWSOME 5-->
    <!--  fontawesome5/css/all.css-->
    <link rel="stylesheet" href="{{ url('assets/fontawesome4/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <link rel="shortcut icon" href= "{{ url('assets/images/favicon.ico') }}" rel="stylesheet">    
    @yield('styles')


</head>

<body>
    @include('layouts.slidebar')
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel" style=" background-repeat: no-repeat; background-image: url({{ url('assets/images/fondo.jpg') }}) ; background-size: cover; ">
        @include('layouts.topmenu')
         <div class="content">
                @yield('content')
        </div>
        
        <div class="clearfix"></div>
        @include('layouts.footer')
    </div>
    <!-- /#right-panel -->

    <!-- Scripts -->
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('assets/js/popper.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.matchHeight.min.js') }}"></script>
   <!-- BORRADO url('assets/js/main.js')  -->

    <!--Local Stuff-->
    @yield('js')
    
</body>
</html>
