<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrador';
        if(Role::where('name', $role->name)->count() == 0 ){
            $role->save();
        }

        $role = new Role();
        $role->name = 'normal';
        $role->description = 'Normal';
        if(Role::where('name', $role->name)->count() == 0 ){
            $role->save();
        }
    }
}
