@extends('layouts.master')
@section('styles')

<!-- DATEPICKER CSS -->
    <link href="{{ url('datepicker_gijgo/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datepicker_gijgo/gijgo.min.css') }}" rel="stylesheet">
<!-- DATATABLE CSS -->    
    <link href="{{ url('datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-life-ring"></i> Lista de comisiones </h3>
        </div>
    </div>

    <div class="clearfix"></div>
  <br>




<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="card-header">
          Comisiones entre periodos<strong></strong>
      </div>

      <div class="card-body card-block"> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="input-group input-daterange">
      <input id="fecha_desde" width="312" />
      <div class="input-group-addon"> hasta </div>
      <input id="fecha_hasta" width="312" />
      <button type="button" id="btnBuscar" class="btn btn-outline-secondary"><span class="fa fa-search"></span></button>
    </div>
    
  </div>
</div>
  <br>      

          @include('partials.flash-message')
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
             
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 15%;">
    <div class="modal-content">
    <div class="modal-header">
        <div class="col text-center">
            <h2 id="modal_titulo"> <b><u> DATOS DE LA COMISION </b></u>  </h2>
        </div>
      </div>
      <div class="modal-body" id = "modal-text" style="font-size:30px; color: black;"></div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="btnEnviar" name="btnGanador" class="btn btn-outline-secondary"> Aceptar </button>      
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')    
<!-- DATATABLE JS -->
<script src="{{ url('datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
<!-- DATEPICKER JS -->
<script src="{{ url('datepicker_gijgo/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/messages.es-es.js') }}" type="text/javascript"></script>

<script>  
  $(document).ready(function() { 
    var fecha = new Date();
    $('#fecha_desde').datepicker({ locale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4', value : dateEs(fecha) });
    $('#fecha_hasta').datepicker({ocale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4',value: dateEs(fecha.setMonth(fecha.getMonth() + 1) ) });
    cargarTabla();
  });
        
  $("#btnBuscar").click(function() {
      $('#datatable-responsive').DataTable().destroy();
      cargarTabla();
   });

function cargarTabla (){
      var table = $('#datatable-responsive').DataTable({
      "responsive" :true,
/*
      "pageLength": 25,
      "processing": true,
      "serverSide": true,    
      "bRetrieve": true,
      "bProcessing": true,
*/
      "ajax": {
            "url": "{{ url('index_ajax') }}",
            "type": "GET",
            "data" : {
            "fecha_desde":  $("#fecha_desde").val(),
            "fecha_hasta":  $("#fecha_hasta").val(),
            }
        },
      "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningun dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Ultimo",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
      "columns": [
                  {"data":"nombre","visible": true, "title" : "Nombre", "orderable": false},
                  {"data":"cargo","visible": true, "title" : "Cargo", "orderable": false},
                  {"data":"cuit","visible": true, "title" : "CUIT", "orderable": false},
                  {"data":"viatico","visible": true, "title" : "Viaticos $", "orderable": true},
                  {"data":"cantidad","visible": true, "title" : "Cantidad", "orderable": true},

                ],
      "createdRow": function( row, data, dataIndex){
            },
      "lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]
  
    });

  

  $('#datatable-responsive_filter').hide();
  $('#datatable-responsive select option:contains("it\'s me")').prop('selected',true); 
  $('#datatable-responsive thead th').each( function (row, i, start, end, display ) {
    if(row != 3 && row != 4 && row != 5){
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Buscar por '+title+'" />' );
    }
  });


  table.columns().every( function () {
      var that = this;

      $( 'input', this.header() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
              that
                  .search( this.value )
                  .draw();
          }
      } );
    });

}

$('#myModal').on('show.bs.modal', function(e) {
    var $modal = $(this);
    var button = $(e.relatedTarget);
    

var fecha_desde = $("#fecha_desde").val();
var fecha_hasta = ($("#fecha_hasta").val());
var personal_id = button.data('id');

/*
var texto_html = '';
var coso = '';

$("#modal_ganador_texto").html('<b> Fecha: </b>' + $("#fecha").val()
+ '<br><b> Resolucion: </b>' + $("#resolucion").val()    
+ '<br><b>Act/exp : </b>' +  $("#exp1").val() + '-' + $("#exp2").val() + '-' + $("#exp3").val() + '-' + $("#exp4").val() + '-' + $("#exp5").val()
+ '<br><b>Localidades :</b>' + $("#localidades option:selected").text()
+ '<br><b>Dias : </b>' +  $("#dias").val()
+ '<br><b>Combustible : $</b>' +  $("#combustible").val() 
+ ' (' + $("#chofer option:selected").text() + ')' );
if($("#externo").val() != 0) {$("#modal_ganador_texto").append('<br><b>Fuera de la provincia: $</b>' +  $("#gastos").val());}
if($("#gastos").val() != 0) {$("#modal_ganador_texto").append('<br><b>Otros gastos : $</b>' +  $("#gastos").val());}
if($("#reintegro").val() != 0) {$("#modal_ganador_texto").append('<br><b>Reintegro : $</b>' +  $("#reintegro").val());}
$("#modal_ganador_texto").append('<br><b>Personal: </b> <br>');
*/

$.ajax({                        
        type: "GET",                 
        url: "{{ URL::to('viatico_confirmacion') }}",                     
        data: {'fecha_desde' : fecha_desde, 'fecha_hasta' : fecha_hasta, 'personal_id' : personal_id}, 
        success: function(result)             
        {
          console.log(result);
/*
            array.forEach(function(element) {
            $("#modal_ganador_texto").append(' - ' + element.empleados + '<BR>');
            });
*/            
        }
    });

});


function dateEs(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}
</script>
@endsection