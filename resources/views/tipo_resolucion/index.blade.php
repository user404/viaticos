@extends('layouts.master')
@section('styles')
<!-- DATATABLE CSS -->    
    <link href="{{ url('datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-life-ring"></i> Lista de tipos de resolucion </h3>
        </div>
    </div>

    <div class="clearfix"></div>
  <br>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="card-header">
          Todas las resoluciones<strong></strong>
      </div>

      <div class="card-body card-block"> 
  <br>      

          @include('partials.flash-message')

          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
             
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalTipoResolucion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Tipo de Resolucion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="formTipoResolucion" method="POST" action="{{ url('tipo_resolucion') }}" accept-charset="UTF-8">
      @csrf
            <div id="mensaje_alerta" class="alert alert-success alert-dismissible" role="alert" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <p> Se agrego el nuevo tipo de resolucion</p>
            </div>

<div class="form-row align-items-center">    
    <div class="col-md-3"> 
    <label> Nombre: </label>
    </div>
    <div class="col-md-6"> 
            <input type="text" class="form-control" id= "nombre" name="nombre" required="">
    </div>
</div>
<br>
<!--CHECKS -->

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="agente" name="agente" type="checkbox" class="form-check-input" value="1"> Agente
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="anio" name="anio" type="checkbox" class="form-check-input" value="1"> Año
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="beneficiario" name="beneficiario" type="checkbox" class="form-check-input" value="1"> Beneficiario
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="dias" name="dias" type="checkbox" class="form-check-input" value="1"> Dias
  </label>
</div>

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="entidad" name="entidad" type="checkbox" class="form-check-input" value="1"> Entidad
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="exp_act" name="exp_act" type="checkbox" class="form-check-input" value="1"> Exp/act
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="obra" name="obra"type="checkbox" class="form-check-input" value="1"> Obra
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="monto" name="monto" type="checkbox" class="form-check-input" value="1"> Monto
  </label>
</div>

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="numero" name="numero" type="checkbox" class="form-check-input" value="1"> Numero
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="programa" name="programa" type="checkbox" class="form-check-input" value="1"> Programa
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="localidad" name="localidad" type="checkbox" class="form-check-input" value="1">Localidad
  </label>
</div>
<!-- END CHECKS -->
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="btnTipoResolucion" type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- END MODAL -->


@endsection

@section('js')    
<!-- DATATABLE JS -->
<script src="{{ url('datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- DATEPICKER JS -->
<script src="{{ url('datepicker_gijgo/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/messages.es-es.js') }}" type="text/javascript"></script>



<script>  
  $(document).ready(function() { 
    var fecha = new Date();

    $('#fecha_desde').datepicker({ locale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4', value : '01-' + '01-' + fecha.getFullYear() });
    $('#fecha_hasta').datepicker({ocale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4',value: dateEs(fecha) });
    cargarTabla();
  });
        
  $("#btnBuscar").click(function() {
      $('#datatable-responsive').DataTable().destroy();
      cargarTabla();
   });


function cargarTabla (fecha_desde, fecha_hasta){

function format ( d ) {
  return 'hola';
//      return d.agente + d.entidad + d.obra + d.beneficiario + d.localidad + d.monto + d.detalle;
}

      var dt = $('#datatable-responsive').DataTable({
      "responsive" :true,
      "ajax": {
            "url": "{{ url('listar_tipos_de_resoluciones') }}",
            "type": "GET",
        },
      "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningun dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Ultimo",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
      "columns": [
                  
                  {"data":"text","visible": true, "title" : "Nombre", "orderable": false},
                  {"data":"act_exp","visible": true, "title" : "Act/exp", "orderable": false},
                  {"data":"tipo_resolucion","visible": true, "title" : "Tipo Res.", "orderable": false},
                  {"data":"agente_entidad","visible": true, "title" : "Agente/Entidad", "orderable": false},
                  {"class":"details-control", "orderable":      false, "data":           'detalle', "render": function ( data, type, full, meta ) {
                        return "<button type='button' class='btn btn-outline-secondary'><span class='fa fa-info'></span> Ver</button>";
                        }, "title" : "Detalle"                      
                  },

                ],
            "initComplete": function(settings, json) {
              //OBTENGO LOS DATOS OBTENIDOS CON JSON Y LOS GUARDO PARA PODER DIBUJARLOS
                datos =  json.data;
            },
      "lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]
  
    });

     // Array to track the ids of the details displayed rows
     var detailRows = [];
// MODIFIQUE EL CODIGO ANTERIOR PARA PODER OBTENER EL INDICE Y ASI PODER ASIGNAR EL DATO CORRECTAMENTE
//    $('#datatable-responsive tbody').on( 'click', 'tr td.details-control', function () {
  $('#datatable-responsive tbody').on( 'click', 'tr', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        var indice = $('#datatable-responsive').DataTable().row( this ).index();

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( datos[indice] ) ).show();
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows

    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );


  $('#datatable-responsive_filter').hide();
  $('#datatable-responsive select option:contains("it\'s me")').prop('selected',true); 
  
  $('#datatable-responsive thead th').each( function (row, i, start, end, display ) {
    if(row != 4 ){
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Buscar por '+title+'" />' );
    }
  });

  dt.columns().every( function () {
      var that = this;

      $( 'input', this.header() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
              that
                  .search( this.value )
                  .draw();
          }
      } );
    });


}
























$('#modal').on('show.bs.modal', function(e) {
  
    var $modal = $(this);
    var button = $(e.relatedTarget);
    
    var fecha_desde = $("#fecha_desde").val();
    var fecha_hasta = ($("#fecha_hasta").val());
    var agente_id = button.data('agente_id');
    
    $('#datatable-responsive').DataTable().destroy();

   
    cargarDetalle(fecha_desde, fecha_hasta, agente_id);


    $(this).find('.modal-content').css({
              width:'120%', //probably not needed
              height:'auto'
             // ,              'max-height':'100%'
       });
       

});


function dateEs(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}
</script>
@endsection