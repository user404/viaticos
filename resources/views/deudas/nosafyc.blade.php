@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')
@auth
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">Pago no SAFYC</div>
        <div class="panel-body">

          @include('partials.flash-message')

          <form method="POST" action={{ url('deuda_no_safyc')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
             <label class="col-md-3 control-label">Periodo:</label>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <select class="form-control" id = "mes" name="mes" required>
                  <option value="" disabled selected>Eliga un mes</option>
                  <option id="ene" value="ene">ENERO</option>
                  <option id="feb" value="feb">FEBRERO</option>
                  <option id="mar" value="mar">MARZO</option>
                  <option id="abr" value="abr">ABRIL</option>
                  <option id="may" value="may">MAYO</option>
                  <option id="jun" value="jun">JUNIO</option>
                  <option id="jul" value="jul">JULIO</option>
                  <option id="ago" value="ago">AGOSTO</option>
                  <option id="sep" value="sep">SEPTIEMBRE</option>
                  <option id="oct" value="oct">OCTUBRE</option>
                  <option id="nov" value="nov">NOVIEMBRE</option>
                  <option id="dic" value="dic">DICIEMBRE</option>
                </select>
            </div>

             <div class="col-md-2 col-sm-2 col-xs-12">
                <select class="form-control" id = "anio" name="anio" required>
                  <option value="" disabled selected>Eliga un año</option>
                  <option id="2018" value="2018">2018</option>
                  <option id="2019" value="2019">2019</option>
                  <option id="2020" value="2020">2020</option>
                  <option id="2021" value="2021">2021</option>
                </select>
            </div>
          </div>

          <br><br>

          <div class="form-group">
              <label class="col-md-3 control-label">Escriba el monto</label>
              <div class="col-md-2">
                <input type="number" class="form-control" name="total" id="total" >
              </div>
            </div>

          <br><br>
            
            <div class="form-group">
             <label class="col-md-3 control-label">Concepto:</label>
            <div class="col-md-4 col-sm-2 col-xs-12">
                <select class="form-control" id = "`jurisdiccion" name="jurisdiccion" required>
                  <option value="" disabled selected>Eliga un concepto</option>
                  <option id="99" value="99">DEBITO EN CUENTA</option>
                  <option id="97" value="97">DEBITO DIRECTO</option>
                  <option id="94" value="94">MUNICIPALIDAD</option>
                  <option id="93" value="93">CAJA MUNICIPAL</option>
                  <option id="91" value="91">SECHEEP</option>
                  <option id="95" value="95">ECOM</option>
                  <option id="98" value="98">CANCELACION</option>
                </select>
            </div>
          </div>

          <br><br>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">Agregar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endauth 
@endsection