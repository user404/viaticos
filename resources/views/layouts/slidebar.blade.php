    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title">Menu Principal</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Res./Comisiones</a>
                        <ul class="sub-menu children dropdown-menu">
                        @if(Auth::check()  && Auth::user()->hasRole('admin'))                          
                            <li><i class="menu-icon fa fa-street-view"></i><a href="{{ url('comision/create') }}">Cargar Resolucion</a></li>
                        @endif    
                            <li><i class="menu-icon fa fa-map-o"></i><a href="{{ url('comision') }}">Listar Comisiones</a></li>
                            <li><i class="menu-icon fa fa-map-o"></i><a href="{{ url('otras_resoluciones') }}">Listar Resoluciones</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Tipo de Resolucion</a>
                        <ul class="sub-menu children dropdown-menu">
                        @if(Auth::check()  && Auth::user()->hasRole('admin'))                          
                            <li><i class="menu-icon fa fa-street-view"></i><a href="{{ url('tipo_resolucion') }}">Listar y editar</a></li>
                        @endif    
                        </ul>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->