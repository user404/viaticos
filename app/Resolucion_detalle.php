<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resolucion_detalle extends Model
{
    public function resolucion()
    {
        return $this->belongsTo('App\Resolucion');
    }

}
