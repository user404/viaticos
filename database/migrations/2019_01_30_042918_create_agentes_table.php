<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->string('cuit')->nullable();
            $table->string('situacion_revista_id')->nullable();
            $table->string('escalafon_id')->nullable();
            $table->string('categoria_id')->nullable();
            $table->string('apartado')->nullable();
            $table->string('cargo_id')->nullable();
            $table->string('ceic')->nullable();
            $table->string('grupo')->nullable();
            $table->string('numero')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentes');
    }
}
