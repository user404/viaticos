<br>

<div class = "row">
  <div class="col-md-6">
  <div class="card">

  <div class="card-header">
      Datos <strong>Comision</strong>
  </div>

      <div class="card-body card-block">
        

      <div class="form-row align-items-center">

        <div class="col-md-2"> 
          <label> Resolucion: </label>
        </div>
        <div class="col-md-3"> 
            <input type="number" class="form-control" id= "resolucion" name="resolucion" value="10" required="">
        </div>  

        <div class="col-md-1"></div>  
      
      <div class="col-md-3">
          <label> Fuera de la provincia? : </label>
        </div>

        <div class="col-auto">
          <input type="checkbox" id="externo" name="externo" value="1"/>
        </div>

      </div>

      <br>

      <div class="form-row align-items-center">

          <div class="col-md-2">
            <label> Act/Exp: </label>
          </div>
          <div class="col-auto">
              <select class="form-control" id="exp1" name="exp1">
                  <option>E</option>
              </select>
          </div>

          <div class="col-auto" style="width:70px">
              <input type="number" class="form-control" id= "exp2" name="exp2" value="10" required="" placeholder="10">
          </div>

          <div class="col-auto" style="width:90px">
              <input type="number" class="form-control" id= "exp3" name="exp3" required="" placeholder="Año" value="2019">
          </div>

          <div class="col-auto" style="width:130px">
              <input type="Number" class="form-control" id= "exp4" name="exp4" required="" placeholder="Numero" value="123">
          </div>

          <div class="col-auto">
              <select class="form-control" id="exp5" name="exp5">
                  <option>A</option>
                  <option>E</option>
              </select>
          </div>    
          
        </div>
        <br>
      
      <div class="form-row align-items-center">
        <div class="col-md-2"> 
          <label> Localidades: </label>
        </div>
        <div class="col-md-5"> 
            <select class="js-example-basic-multiple form-control" id= "localidades" name="localidades[]" multiple="multiple">
                @foreach($localidades as $l)
                    <option value="{{$l->id}}">{{$l->nombre}}</option>
                @endforeach
            </select>        
        </div>

        <div class="col-md-1"> 
          <label> Dias: </label>
        </div>
        <div class="col-md-2"> 
            <input type="number" class="form-control" id= "dias" name="dias" required="" value= "1">
        </div>

      </div>

  </div>
    </div>
  </div>

<!-- FIN DE COLUMNA DE 6 -->

  <div class="col-md-6">
  <div class="card">
<div class="card-header">
    Gastos <strong>Comision</strong>
</div>

    <div class="card-body card-block">
      

    <div class="form-row align-items-center">
      <div class="col-md-2"> 
        <label> Combustible: </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "combustible" name="combustible" required="" value="2">
      </div>  


    </div>

    <br>

    <div class="form-row align-items-center">
      <div class="col-md-2"> 
        <label> Otros gastos: </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "gastos" name="gastos" required="" value="0">
      </div>
    </div>

    <br>

    <div class="form-row align-items-center">
    
        <div class="col-md-2">
            <label> Es reintegro?: </label>
        </div>

        <div class="col-auto" style="width:120px">
            <input type="checkbox" id="check_reintegro" name="check_reintegro" value="0"/>
        </div>


      <div class="col-md-2" id="lbl_monto_reintegro"> 
        <label> Monto: </label>
      </div>

      <div class="col-md-3"> 
          <input type="number" class="form-control" id= "reintegro" name="reintegro" value="0" required="">
      </div> 
    </div>

</div>
</div>
</div>


</div>





<div class="card">
<div class="card-header">
    Datos <strong>Empleados</strong>
</div>
<div class="card-body card-block">

<div class="form-row align-items-center" >
        <div class="col-md-2"> 
          <label> Personas: </label>
        </div>
        <div class="col-md-4">
        <select class="js-example-basic-multiple form-control" id= "personal" name="personal[]" multiple="multiple">
            @foreach($personal as $l)
                <option value="{{$l->id}}">{{$l->nombre}}</option>
            @endforeach
        </select>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-1"> 
          <label> Chofer: </label>
        </div>


        <div class="col-md-3"> 
            <select class="form-control" id= "chofer" name="chofer">
            @foreach($personal as $l)
                <option value="{{$l->id}}">{{$l->nombre}}</option>
            @endforeach
        </select>
        </div>
      </div>
              <br><br>

        <button type="button" class="btn btn-outline-secondary float-left" data-toggle="modal"  data-target="#myModalGanador">MODAL</button>

        <button type="submit" class="btn btn-outline-secondary float-right">Aceptar</button>
    </div>
    
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-user"></i> Login
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</div>



