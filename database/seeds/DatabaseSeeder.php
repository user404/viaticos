<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//		$this->call(UsersTableSeeder::class);
        $this->call(CargosTableSeeder::class);
		    // La creación de datos de roles debe ejecutarse primero
		$this->call(RoleTableSeeder::class);
		// Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);
        
        $this->call(ProvinciasTableSeeder::class);

        $this->call(LocalidadesTableSeeder::class);

        $this->call(TipoResolucionTableSeeder::class);

//        $this->call(AgenteTableSeeder::class);
        

    }
}
