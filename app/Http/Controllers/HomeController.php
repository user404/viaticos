<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $path = 'comision/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return ($this->path.'.dashboard');
//        return redirect()->action('DeudaController@index', ['id' => 0]);
//      return view($this->path);
        return redirect()->action('ComisionController@index');
    }
}
