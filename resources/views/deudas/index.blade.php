<!-- escritura/index  -->
@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_content">
      <div class="x_panel">
              <h2>{{$titulo}}<small></small></h2> 

         @include('partials.flash-message')

          <div class="x_title">
          </div>
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
      </div>
    </div>
  </div>
</div>  

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <form id = "myform" method = "post" action = "">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="modal-content">
      <div class="modal-header">
        <h2 id="modal_titulo"></h2>
      </div>

      <div class="modal-body">
        <h5 class="modal-title" id="titulo_modal">Seleccione una deuda para el pago</h5>        
        <input type="hidden" id = "id_nota_credito" name="id_nota_credito" value="">

        <input type="hidden" id = "monto" name="monto" value="">

        <input type="hidden" id = "monto_" name="monto" value="">
        
        <p id= 'txtNumEsc'> </p>
          <div class="form-group">
            <label class="col-md-3 control-label">Periodo/Efectivizada:</label>
            <div class="col-md-4 col-sm-2 col-xs-12">
                <select class="form-control" id = "periodo_deuda" name="periodo_deuda" required></select>
            </div>
            <br><br>
            <input type="hidden" id ="monto_safyc" name="monto_safyc" value="">
            <input type="hidden" id ="monto_deuda" name="monto_deuda" value="">
            <p id='pendiente' name='pendiente'> </p>
          </div>

          <div id="div_generar_num">

          <label class="radio-inline">
            <input id ="generar_num" name="generar_num" type="radio" value="Si" checked="checked"> Si
          </label>
          <br>
          <label class="radio-inline">
            <input id ="generar_num" name="generar_num" type="radio" value="No" > No
          </label>
        </div>

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Asignar</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalBorrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <form id = "myformborrar" method = "POST" action={{ url('archivo')}}>
        <input type="hidden" name="_method" value="delete" />
    {!! csrf_field() !!}
    <div class="modal-content">
      <div class="modal-header">
        <h2 id="modal_borrar_titulo"></h2>
      </div>
      <div class="modal-body">   
        <p> Al borrar el archivo del servidor no podra recuperarlo, seguro que desea continuar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-danger">Borrar</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalComentarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post", action="/comentario">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comentarios</h5>
      </div>
      <div class="modal-body">
        <div class="x_content">
      <div class="x_panel">
              <h5 id = "modal_comentario_titulo">Nuevo comentario<small></small></h5>
          <div class="x_title">
          </div>
        <input type="hidden" id = "archivo_id" name="archivo_id" value="">
          <p id= 'txtNumEsc'> </p>
          <label>Comentario:</label>
          <textarea rows="2" cols="50" name="descripcion" id="descripcion" class="form-control" required></textarea>
          <br>
          <button type="submit" class="btn btn-primary" style="float: right;"> Agregar</button>
      </div>
    </div>

        <h5>Lista de comentarios</h5>
         <table id="datatable-responsive-comments" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
      </div><br>
        <br>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    </form>
  </div>
</div>


@endsection

@section('javascripts')
<script type="text/javascript" src="{{ url('datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('datatables/responsive/js/dataTables.responsive.min.js') }}"></script>



<script>

  $('#div_generar_num').hide();

  $(document).ready(function() {
    var table = $('#datatable-responsive').DataTable({
//      '{{ URL::asset('/images/flags/') }}' + $("select#lang").val() + '.png)');
      "responsive" :true,
      "ajax": "{{ url('ajax_deuda'," $deuda ") }}",
//      "language": {"url": "/datatables/idiomatabla.json"},
      "columns": [

                          {"data":"jurisdiccion","visible": true, "title" : "Jurisdiccion"},
                          {"data":"nro_ent","visible": true, "title" : "N_Ent"},
//                          {"data":"pago_id","visible": true, "title" : "Pago_id"},
                          {"data":"fec_generado","visible": true, "title" : "F_Generado"},
//                          {"data":"entregado","visible": true, "title" : "Entregado"},
//                          {"data":"fec_entregado","visible": true, "title" : "F_Entregado"},
//                          {"data":"anulado","visible": true, "title" : "Anulado"},
//                          {"data":"revertido","visible": true, "title" : "Revertido"},
                          {"data":"nro_expediente","visible": true, "title" : "Nro_Exp"},
                          {"data":"periodo","visible": true, "title" : "Periodo_Deuda"},
//                          {"data":"beneficiario","visible": true, "title" : "Beneficiario"},
                            {"data":"monto","visible": true, "title" : "Monto"}
/*
                            ,{
                            "data": null,
                            "render": function ( data, type, full, meta ) {                                  
                                    var str = 'archivo/' + data.id;
                                    return "<button id='btnPDF' value='"+str+"' type='buttom' class='btn btn-info btn-flat' width='30px' width='30px'><i class='glyphicon glyphicon-list-alt'></i>";}
                                , "title" : "Ver"}
*/
                         ,{
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                    var str = "{{ URL::to('pdf/create/ID') }}";
                                    var res = str.replace("ID", data.id);
                                    return "<button id='btnModal' type='buttom' data-toggle='modal' data-id_safyc=' " + data.id + " ' data-id_jurisdiccion=' " + data.jurisdiccion_id + " ' data-monto= ' " + data.monto + " ' data-target='#myModal' class='btn btn-info btn-flat' width='30px' ><i class='glyphicon glyphicon-arrow-right'></i></a>";}
                                , "title" : "Asignar"},

  /*                        ,{
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                    return "<button id='btnModalComentarios' type='buttom' data-toggle='modal' data-id=' " + data.id + " ' data-nombre=' " + data.nombre + " ' data-target='#myModalComentarios' class='btn btn-info btn-flat' width='30px' ><i class='glyphicon glyphicon-list-alt'></i></a>";}
                                , "title" : "Comentarios"}          
*/                                                     
/*
                              "render": function ( data, type, full, meta ) {
                                    var str = "{{ URL::to('pdf/create/ID') }}";
                                    var res = str.replace("ID", data.id);
                                    return "<a href='"+res+"' type='buttom' class='btn btn-info btn-flat' width='30px' ><i class='fa fa-print'></i></a>";}
                                , "title" : "Exportar"}
*/                                                                
                         
                ],
      "lengthMenu": [[ 10, 25, -1], [ 10, 25, "TODOS"]]
    });



  });


  

$('#myModal').on('show.bs.modal', function(e) { 
//   $("#pago").text('');
  $('#div_generar_num').hide();
    var $modal = $(this);
    var button = $(e.relatedTarget);
    var id_jurisdiccion = button.data('id_jurisdiccion');
    var id_safyc = button.data('id_safyc');
    
    var nombre = button.data('nombre');
    var str = "{{ URL::to('deuda_periodo/ID') }}";
    var res = str.replace("ID", id_jurisdiccion);

    var monto_safyc = button.data('monto');
    $('#monto_safyc').val(monto_safyc);
    $('#titulo_modal').text('Seleccione una deuda para el pago $' + monto_safyc);
    

//    alert(monto_safyc); 

    $("#pendiente").text('');

//    $("#pago").text('Quedan pendientes: $' + monto_safyc);
    
//    alert(monto);

    $.ajax({
        type: 'get',
        url: res, 
        success: function (data) {
            var $periodo_deuda = $('#periodo_deuda');
            $periodo_deuda.empty();
            $periodo_deuda.append('<option value="" disabled selected>Eliga una deuda</option>');
            for (var i = 0; i < data.length; i++) {
                $periodo_deuda.append('<option id=' + data[i].id + ' value=' + data[i].id + '>' + data[i].nombre + '</option>');
            }
        }
    });
    // USO REPLACE PARA QUITAR LOS ESPACION EN BLANCO DE LA CADENA DE STRINGS
    var url_asignar = "{{ URL::to('asignar_deuda/ID') }}";
    var res_asignar = url_asignar.replace("ID", id_safyc);

    $('#myform').attr("action", res_asignar);
    $("#id_nota_credito").val(id_safyc);
    $("#modal_titulo").text(nombre);
})

$("#periodo_deuda").change(function() {  
  $('#div_generar_num').hide();
//.find(":selected").text();
  
  var monto_deuda = $('#periodo_deuda').find(":selected").text().substr(10);
  // .substr(10);
  var monto_safyc =  $('#monto_safyc').val()
  
  var restante = monto_deuda - monto_safyc;
  if (restante == 0 )
  {
    $('#div_generar_num').show();
    $("#pendiente").text('Asignando este pago saldaria la deuda. Desea generar nota de credito?');

  }
  else
  {
    $("#pendiente").text('Quedarian pendientes: $' + restante);  
  }

});

$('#myModalBorrar').on('show.bs.modal', function(e) {
    var $modal = $(this);
    var button = $(e.relatedTarget);
    var id = button.data('id');
    var nombre = button.data('nombre');
    $('#myformborrar').attr("action", 'archivo/' + id.replace(/\s/g, ''));
    $("#id_borrar").val(id);
    $("#modal_borrar_titulo").text('Seguro desea borrar el archivo:' + nombre + '?');
})



$('#myModalComentarios').on('show.bs.modal', function(e) {
var $modal = $(this);
var button = $(e.relatedTarget);
var id = button.data('id');

var url = "{{ URL('comentario', 'ID') }} ";
var res = url.replace("ID", id.trim());
console.log(res);
  var table = $('#datatable-responsive-comments').DataTable({
        "responsive" :true,
        "ajax": res ,
        "columns": [
                          {"data":"creado","visible": true, "title" : "Creado"}, 
                          {"data":"usuario","visible": true, "title" : "Usuario"},
                          {"data":"descripcion","visible": true, "title" : "Comentario"}                          
                ],
      "lengthMenu": [[ 10, 25, -1], [ 10, 25, "TODOS"]]
    });

    var $modal = $(this);
    var button = $(e.relatedTarget);
    var archivo_id = button.data('id');
    var nombre = button.data('nombre');
    $("#archivo_id").val(id);
    $("#modal_comentario_titulo").text('Nuevo comentario para :' + nombre);


})

$("#myModalComentarios").on("hidden.bs.modal", function () {
  $('#datatable-responsive-comments').empty();     
  var table = $('#datatable-responsive-comments').DataTable();
  table.destroy(); 
});

  $(document).on("click", "#btnPDF", function () {
     var w = window.open(this.value, 'popUpWindow','height=600,width=800,left=10,top=10,,scrollbars=yes,menubar=no'); return false;
});

</script>
@endsection