@extends('layouts.master')
@section('styles')
    <link href="{{ url('select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ url('select2/estilo/dist/select2-bootstrap4.css') }}" rel="stylesheet">
    <!-- DATEPICKER CSS -->
    <link href="{{ url('datepicker_gijgo/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('datepicker_gijgo/gijgo.min.css') }}" rel="stylesheet">
<style>
*{
        font-size: 12.5px;
}
</style>
@endsection


@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-life-ring"></i> Nueva Resolucion </h3>
        </div>
        @include('/partials/flash-message')
    </div>

    <div class="clearfix"></div>

<form id="form" method="POST" action="{{ url('comision') }}" accept-charset="UTF-8">
@csrf
<br>

<div class = "row">  
        <div class="col-md-12">
        <div class="card">
      
        <div class="card-header">
            Datos <strong>Resolucion</strong>
        </div>
      
            <div class="card-body card-block">

            <div id = "error_resolucion_existente" class="alert alert-danger" role="alert"  style="display:none"> 
                <button type="button" id="closeAlert" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>    
                <p id= "error_resolucion_existente_texto"></p>
            </div>  
      
            <div class="form-row align-items-center">    

            <div class="col-md-1"> 
                <label> Tipo Res.: </label>
            </div>
      
            <div class="col-md-3"> 
            <select class="js-example-basic-single" id="tipo_resolucion" name="tipo_resolucion" style="width: 80%">
                <option disabled selected value> </option>
                    @foreach($tipo_resolucion as $l)
                        <option value="{{$l->id}}">{{$l->nombre}}</option>
                    @endforeach
            </select>
            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modalTipoResolucion"><span class="fa fa-plus"></span></button>
            </div>
      
            <div class="col-md-1"> </div>
      
              <div class="col-md-1"> 
                <label> Resolucion: </label>
              </div>
              <div class="col-md-1"> 
                      <input type="number" class="form-control" id= "resolucion" name="resolucion" value= "{{ $resolucion->numero or '' }}" required="">
              </div>
              
      
              <div class="col-md-1"></div>  
                              
              <div class="col-md-1">
                  <label> Fecha res.: </label>
              </div>
              <div class="col-md-2"> 
              <input id="fecha" name="fecha" value ="@if( isset($resolucion->fecha) ){{ Carbon\Carbon::parse($resolucion->fecha)->format('d-m-Y') }}@endif"/>
              </div>
      
            </div>
      
            <br>
        </div>
          </div>
        </div>
</div>

<div class = "row" id="otras_resoluciones" style="display:none">  
        <div class="col-md-12">
        <div class="card">
      
        <div class="card-header">
            Detalle <strong>Resolucion</strong>
        </div>
      
            <div class="card-body card-block">

            <div class = "row">  

              <div class="col-md-1" id="agente_lbl_div"> 
                <label> Agente: </label>
              </div>
              <div class="col-md-3" id="agente_div"> 
              <select class="js-example-basic-single" id= "agente_ot" name="agente_ot" style="width: 100%">
                    <option disabled selected value> </option>
                    @foreach($agentes as $l)
                        <option value="{{$l->id}}">{{ $l->nombre }}</option>
                    @endforeach
                    </select>
              </div>

              <div class="col-md-1" id="entidad_lbl_div"> 
                <label> Entidad: </label>
              </div>
              <div class="col-md-2" id="entidad_div"> 
                      <input type="text" class="form-control" id= "entidad_ot" name="entidad_ot">
              </div>

              <div class="col-md-1" id="obra_lbl_div"> 
                <label> Obra: </label>
              </div>
              <div class="col-md-2" id="obra_div"> 
                      <input type="text" class="form-control" id= "obra_ot" name="obra_ot">
              </div>

              <div class="col-md-1" id="programa_lbl_div"> 
                <label> Programa: </label>
              </div>
              <div class="col-md-2" id="programa_div"> 
                      <input type="text" class="form-control" id= "programa_ot" name="programa_ot">
              </div>

              <div class="col-md-1" id="dias_lbl_div"> 
                <label> Dias: </label>
              </div>
              <div class="col-md-2" id="dias_div"> 
                      <input type="text" class="form-control" id= "dias_ot" name="dias_ot">
              </div>


              <div class="col-md-1" id="numero_lbl_div"> 
                <label> Numero: </label>
              </div>
              <div class="col-md-2" id="numero_div"> 
                      <input type="text" class="form-control" id= "numero_ot" name="numero_ot">
              </div>


                     <div class="col-auto" style="width:130px">
                      <label> Act/Exp: </label>
                    </div>
          
                    <div class="col-auto">
                        <select class="form-control" id="exp1_ot" name="exp1_ot">
                            <option>E</option>
                        </select>
                    </div>
          
                    <div class="col-auto" style="width:95px">
                        <input type="number"  class="form-control" id= "exp2_ot" name="exp2_ot" value="10" placeholder="10">
                    </div>
          
                    <div class="col-auto" style="width:110px">
                        <input type="number"  class="form-control" id= "exp3_ot" name="exp3_ot" placeholder="Año" value="2019">
                    </div>
          
                    <div class="col-auto" style="width:130px">
                        <input type="Number"  class="form-control" id= "exp4_ot" name="exp4_ot" placeholder="Numero" value="">
                    </div>
          
                    <div class="col-auto">
                        <select class="form-control"  id="exp5_ot" name="exp5_ot">
                            <option>A</option>
                            <option>E</option>
                        </select>
                    </div>
                    
          

              <div class="col-md-1" id="beneficiario_lbl_div"> 
                <label> Beneficiario: </label>
              </div>
              <div class="col-md-2" id="beneficiario_div"> 
                      <input type="text" class="form-control" id= "beneficiario_ot" name="beneficiario_ot">
              </div>

              <div class="col-auto" id="localidad_lbl_div"> 
                <label> Localidad: </label>
              </div>
              <div class="col-md-3" id="localidad_div"> 
                <select class="js-example-basic-single" id= "localidad_ot" name="localidad_ot" style="width: 100%">
                <option disabled selected value> </option>
                    @foreach($localidades as $l)
                        <option value="{{$l->id}}">{{$l->nombre}}</option>
                    @endforeach
                </select>
              </div>

              <div class="col-auto" id="monto_lbl_div"> 
                <label> Monto: </label>
              </div>
              <div class="col-md-2" id="monto_div"> 
                      <input type="text" class="form-control" id= "monto_ot" name="monto_ot">
              </div>

              </div>

                <br>
              <div class = "row" id="detalle_div">
                <div class="col-md-1"> 
                    <label> Detalle: </label>
                </div>
                <div class="col-md-3"><textarea class="form-control" rows="1" id="detalle_ot" name="detalle_ot"></textarea></div>
            </div>
            
        </div>
          </div>
          <button id="btnOtrasResoluciones" type="button" class="btn btn-secondary float-right">Aceptar</button>

        </div>
</div>


<div class = "row" id ="comisiones">  
    <!-- COMIENZO DE COLUMNAS DE 6 -->

<div class="col-md-6">
  <div class="card">
    <div class="card-header">
        Datos <strong>Comision</strong>
    </div>
    <div class="card-body card-block">
    <div id = "error_comision" class="alert alert-danger" role="alert" style="display:none"> <p id= "text_error_comision"> <b>ERROR:</b> Debe seleccionar una provincia/localidad para continuar'</p></div>
            <div class="form-row align-items-center">
                <div class="col-md-2">
                        <label> Fec. Salida. : </label>
                    </div>
                    <br><br>
                    
                    <div class="col-md-3"> 
                        <input id="fecha_salida" name="fecha_salida" value ="@if( isset($comision->fecha_salida) ){{ Carbon\Carbon::parse($comision->fecha_salida)->format('d-m-Y') }}@endif"/>
                    </div>
                    <div class="col-md-1"> </div>
                    
                    <div class="col-md-1"> 
                            <label> Dias: </label>
                          </div>
                          <div class="col-md-2"> 
                              <input type="number" class="form-control" id= "dias" name="dias" required="" value= "1">
                          </div>
            </div>
            <br>
            <div class="form-row align-items-center">
                    <div class="col-md-2">
                      <label> Act/Exp: </label>
                      <input type="checkbox" id="act_exp" name="act_exp" value="1"/> 
                    </div>
                    
                    <div class="col-auto">
                        <select class="form-control" id="exp1" name="exp1">
                            <option>E</option>
                        </select>
                    </div>
          
                    <div class="col-auto" style="width:70px">
                        <input type="number" disabled class="form-control" id= "exp2" name="exp2" value="10" placeholder="10">
                    </div>
          
                    <div class="col-auto" style="width:90px">
                        <input type="number" disabled class="form-control" id= "exp3" name="exp3" placeholder="Año" value="2019">
                    </div>
          
                    <div class="col-auto" style="width:180px">
                        <input type="text" disabled class="form-control" id= "exp4" name="exp4" placeholder="Numero" value="">
                    </div>
          
                    <div class="col-auto">
                        <select class="form-control" disabled id="exp5" name="exp5">
                            <option>A</option>
                            <option>E</option>
                        </select>
                    </div>
                    
                    <div class="col-md-1">
                    </div>        
            </div>
            <br>

        <div class="form-row align-items-center">
            <div class="col-md-2">
                <label> Fuera de la provincia? : </label>
            </div>
            <div class="col-md-1">
                <input type="checkbox" id="externo" name="externo" value="1"/>
            </div>  
            
            <div class="col-md-2"> 
                <label id="destino"> Localidades : </label>
            </div>
            <div class="col-md-4"> 
                <div id='div_localidades'>
                    <select class="js-example-basic-multiple form-control" id= "localidades" name="localidades[]" multiple="multiple">
                        @foreach($localidades as $l)
                            <option value="{{$l->id}}">{{$l->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div id='div_provincias'  style="display:none">
                    <select class="js-example-basic-single" id="provincias" name="provincias" style="width: 100%">
                    <option disabled selected value> </option>
                        @foreach($provincias as $l)
                            <option value="{{$l->id}}">{{$l->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <br>

        <div class="form-row align-items-center" >
                <div class="col-md-3">
                    <label> Comision Pasaje: </label>
                </div>
                <div class="col-md-1">
                    <input type="checkbox" id="check_pasaje" name="check_pasaje" value="1"/>
                </div>  
                
                <div class="col-md-2">
                    <label> Auto </label>
                </div>
                <div class="col-md-1">
                    <input type="checkbox" id="check_vehiculo" name="check_vehiculo" checked="checked"/>
                </div> 
        </div>
        <br>

        <div class="form-row align-items-center">
            <div class="col-md-2">
                <label> Posee gastos extra? : </label>
            </div>
            <div class="col-md-1">
                <input type="checkbox" id="check_gastos" name="check_gastos" value="1"/>
            </div>   
        </div>


</div>
</div>
</div>

<div class="col-md-6">
   
    <div class="card" id="div_gastos_pasaje" style ="display : none;">
        <div class="card-header">
            Gastos viaje<strong> Avion/Colectivo</strong>
        </div>
        <div class="card-body card-block">
            <div class="form-row align-items-center" >              
                <div class="col-md-2" id="lbl_agente_pasaje"> 
                    <label> Agente: </label>
                </div>
                <div class="col-md-6">
                    <select class="js-example-basic-multiple form-control" id= "agente_pasaje" name="agente_pasaje[]" multiple="multiple" style="width: 100%">
                        @foreach($agentes as $l)
                            <option value="{{$l->id}}">{{$l->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <br>
        </div>
    </div>
       
    

     <div class="card" id="div_gastos_vehiculo">
        <div class="card-header">
            Datos <strong>Agentes en vehiculo</strong>
        </div>
        <div class="card-body card-block">

            <div class = "form-row align-items-center">  
                <div class="col-auto" >
                    <label> Marca y modelo: </label>
                </div>
                <div class="col-auto"> 
                        <input type="text" class="form-control" id= "marca_modelo" name="marca_modelo"  placeholder="EJ: Honda FIT 2012">
                </div>
            </div>
<br>            
            <div class = "form-row align-items-center">
                <div class="col-auto" >
                    <label> Patente: </label>
                </div>
                <div class="col-auto"> 
                        <input type="text" class="form-control" id= "patente" name="patente">
                </div>

                <div class="col-md-3"> 
                    <label> Combustible ($): </label>
                </div>

                <div class="col-md-3"> 
                    <input type="number" step="0.01" class="form-control" id= "combustible" name="combustible" value="0">
                </div>             
            </div>
<br>
            
                <div class="form-row align-items-center">                

                        <div class="col-md-2"> 
                          <label> Agente: </label>
                        </div>
                        <div class="col-md-6">
                        <select class="js-example-basic-multiple form-control" id= "agente" name="agente[]" multiple="multiple" style="width: 100%">
                            @foreach($agentes as $l)
                                <option value="{{$l->id}}">{{$l->nombre}}</option>
                            @endforeach
                        </select>
                        </div>
                        <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modalAgente"><span class="fa fa-plus"></span></button>
                </div>
                <br>
                <div class="form-row align-items-center" >
                    <div class="col-md-2" id= "lbl_chofer"> 
                        <label> Chofer: </label>
                    </div>
                    <div class="col-md-6" id= "div_chofer"> 
                        <select class="js-example-basic-single form-control" id= "chofer" name="chofer" style="width: 100%">
                        <option disabled selected value> </option>
                        @foreach($agentes as $l)
                            <option value="{{$l->id}}">{{ $l->nombre }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>

        </div>

    </div>
    <button type="button" class="btn btn-secondary float-right"  data-target="#myModalGanador" id="btnModal">Aceptar</button>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('assets/images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 15%;">
    <div class="modal-content">
    <div class="modal-header">
        <div class="col text-center">
            <h2 id="modal_titulo"> <b><u> DATOS DE LA COMISION </b></u>  </h2>
            <div id = "error_resolucion" class="alert alert-danger" role="alert" style="display:none"><p id= "text_error_resolucion"></p></div>
        </div>
      </div>
      
      <div class="modal-body" id = "modal_ganador_texto" style="font-size:30px; color: black;"></div>      
      
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" id="btn_modal" id="btnEnviar1" name="btnGanador" class="btn btn-outline-secondary"> Aceptar </button>      
      </div>
    </div>
  </div>
</div>
</form>
<!-- End Modal -->

<!-- Modal -->
<div class="modal fade" id="modalTipoResolucion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Tipo de Resolucion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="formTipoResolucion" method="POST" action="{{ url('tipo_resolucion') }}" accept-charset="UTF-8">
      @csrf
            <div id="mensaje_alerta" class="alert alert-success alert-dismissible" role="alert" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <p> Se agrego el nuevo tipo de resolucion</p>
            </div>

<div class="form-row align-items-center">    
    <div class="col-md-3"> 
    <label> Nombre: </label>
    </div>
    <div class="col-md-6"> 
            <input type="text" class="form-control" id= "nombre" name="nombre" required="">
    </div>
</div>
<br>
<!--CHECKS -->

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="agente" name="agente" type="checkbox" class="form-check-input" value="1"> Agente
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="anio" name="anio" type="checkbox" class="form-check-input" value="1"> Año
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="beneficiario" name="beneficiario" type="checkbox" class="form-check-input" value="1"> Beneficiario
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="dias" name="dias" type="checkbox" class="form-check-input" value="1"> Dias
  </label>
</div>

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="entidad" name="entidad" type="checkbox" class="form-check-input" value="1"> Entidad
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="exp_act" name="exp_act" type="checkbox" class="form-check-input" value="1"> Exp/act
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="obra" name="obra"type="checkbox" class="form-check-input" value="1"> Obra
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="monto" name="monto" type="checkbox" class="form-check-input" value="1"> Monto
  </label>
</div>

<div class="form-check-inline">
  <label class="form-check-label">
    <input id="numero" name="numero" type="checkbox" class="form-check-input" value="1"> Numero
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="programa" name="programa" type="checkbox" class="form-check-input" value="1"> Programa
  </label>
</div>
<div class="form-check-inline">
  <label class="form-check-label">
    <input id="localidad" name="localidad" type="checkbox" class="form-check-input" value="1">Localidad
  </label>
</div>
<!-- END CHECKS -->
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="btnTipoResolucion" type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- END MODAL -->



<!-- Modal -->
<div class="modal fade" id="modalAgente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Nuevo agente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form id="formAgente" method="POST" action="{{ url('agente') }}" accept-charset="UTF-8">
        @csrf
              <div id="mensaje_alerta_agente" class="alert alert-success alert-dismissible" role="alert" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <p> Se agrego el nuevo agente</p>
              </div>
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Nombre: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_nombre" required="">
      </div>
  </div>
  <br>  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> CUIT: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_cuit" required="">
      </div>
  </div>
  <br>   
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Siutacion_revista: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_situacion_revista_id" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Escalafon: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_escalafon_id" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Categoria: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_categoria_id" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Apartado: </label>
      </div>
      <div class="col-md-7"> 
              <input type="text" class="form-control" name="agente_apartado" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Cargo: </label>
      </div>
      <div class="col-md-7"> 
          <select class="js-example-basic-single" id="agente_cargo_id" name="agente_cargo_id" style="width: 100%">
              <option disabled selected value> </option>
              @foreach($cargos as $c)
                  <option value="{{$c->id}}">{{$c->nombre}}</option>
              @endforeach
          </select>
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Ceic: </label>
      </div>
      <div class="col-md-7"> 
              <input type="number" class="form-control" name="agente_ceic" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Grupo: </label>
      </div>
      <div class="col-md-7"> 
              <input type="number" class="form-control" name="agente_grupo" required="">
      </div>
  </div>
  <br> 
  
  <div class="form-row align-items-center">    
      <div class="col-md-3"> 
      <label> Numero: </label>
      </div>
      <div class="col-md-7"> 
              <input type="number" class="form-control" name="agente_numero" required="">
      </div>
  </div>
  <br> 
  
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button id="btnAgente" type="button" class="btn btn-primary">Aceptar</button>
        </div>
      </div>
    </div>
  </div>
  </form>
  <!-- END MODAL -->


@endsection


@section('js')



<script src="{{ url('select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ url('datepicker_gijgo/messages.es-es.js') }}" type="text/javascript"></script>

<script src="{{ url('funciones.js') }}" type="text/javascript"></script>



<script>

$.ajax({                        
    type: "GET",                 
    url: "{{ URL::to('listar_tipos_de_resoluciones') }}",                     
    success: function(array) {
        array.forEach(function(element) { 
            arreglojson.push(element)
        });
    }
}).responseJSON;

$("#tipo_resolucion").change(function() { 

    if($('#tipo_resolucion option:selected').text() == "COMISIÓN"){
        $('#comisiones').show();
        $('#otras_resoluciones').hide(); 
    } else {
        $('#comisiones').hide();
        $('#otras_resoluciones').show(); 
    }
    var searchTerm = $('#tipo_resolucion option:selected').text();

 
    $("#agente_div").hide();
    $("#agente_lbl_div").hide();
    var searchTerm = $('#tipo_resolucion option:selected').text();

    for (var i = 0; i < arreglojson.length; i++){
        if (arreglojson[i].text == searchTerm){
            for (var valor in arreglojson[i]) {
                if(arreglojson[i][valor] == 0){  
                    $('#' + valor + '_div').hide();   
                    $('#' + valor + '_lbl_div').hide();  
                } else {
                    $('#' + valor + '_div').show();   
                    $('#' + valor + '_lbl_div').show();
                }                
            }
        }
    }
        
})


var arreglojson = [];


$( "#btnTipoResolucion" ).click(function() {

    var url = "{{ URL::to('tipo_resolucion') }}";
    var ultimo;
   
   $.ajax({
        type: "post",                 
        url: url,                     
        data: $("#formTipoResolucion").serialize(), 
        success: function(result)             
        {
            arreglojson = [];

            result.lista_full.forEach(function(element) { 
                arreglojson.push(element)
            });

            $('#mensaje_alerta').toggle(3000);
            setTimeout(function ()
            {
                $('#modalTipoResolucion').modal('toggle');         
            }, 3700);

            ultimo = $("#nombre").val();

            $('#tipo_resolucion').select2({
                data: result.lista_select,
                theme: 'bootstrap4',  
                allowClear: true
                });

            $('#tipo_resolucion option').eq(result.lista_select.length).prop('selected', true);
            $('#tipo_resolucion').trigger('change');                
        }
       });
});


$(document).ready(function() {
    $('#tipo_resolucion option:contains("COMISIÓN")').prop('selected',true);

    $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    
    $('#fecha').datepicker({ locale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4' });
    $('#fecha_salida').datepicker({ locale: 'es-es', format: 'dd-mm-yyyy', uiLibrary: 'bootstrap4' });

    $('.js-example-basic-multiple').select2({ theme: 'bootstrap4', placeholder: "Elija una opcion", allowClear: true});
    $('.js-example-basic-single').select2({ theme: 'bootstrap4', placeholder: "Elija una opcion", allowClear: true });

});


$("#check_vehiculo").change(function(){
    ($("#check_vehiculo").is(":checked")) ?  $("#div_gastos_vehiculo").show()  : $("#div_gastos_vehiculo").hide();
});
    
$("#check_pasaje").change(function(){
    ($("#check_pasaje").is(":checked")) ?  $("#div_gastos_pasaje").show()  : $("#div_gastos_pasaje").hide();
});

$("#act_exp").change(function() {    
    ( $("#act_exp").is(":checked") ) ? $("#exp1").removeAttr('disabled') : $("#exp1").attr('disabled','disabled');
    ( $("#act_exp").is(":checked") ) ? $("#exp2").removeAttr('disabled') : $("#exp2").attr('disabled','disabled');
    ( $("#act_exp").is(":checked") ) ? $("#exp3").removeAttr('disabled') : $("#exp3").attr('disabled','disabled');
    ( $("#act_exp").is(":checked") ) ? $("#exp4").removeAttr('disabled') : $("#exp4").attr('disabled','disabled');
    ( $("#act_exp").is(":checked") ) ? $("#exp5").removeAttr('disabled') : $("#exp5").attr('disabled','disabled');    
})

$("#agente").change(function() {
    var str_array = $('#agente').val();
    var array = $('#agente option:not(:selected)');


    var $dropdown = $("#chofer");
    $dropdown.empty();
    $.each(array, function() {
        $dropdown.append($("<option />").val(this.value).text(this.text));
    });

    $("#chofer").val("null").trigger("change");
//    $('#chofer').select2('open');  
    });

    $(document).on('focus', '.select2', function (e) {
        if (e.originalEvent) {
        $(this).siblings('select').select2('open');    
        } 
    });

$("#btnModal").click(function(){

    if( ($("#externo").is(':not(:checked)')  && ( $("#localidades option:selected").text().length < 2 ) )  || ($("#externo").is(":checked") && ( $("#provincias option:selected").text().length < 2 ) ) )  { 
        $("#provincias").focus();
        $("#error_comision").show();
    } else {
    
        $("#error_comision").hide();
        $('#myModalGanador').modal("show");
        $('#btnModal').prop('disabled', false);
    }
})


$("#externo").change(function(){
    if($("#externo").is(":checked")){ 
        $('#destino').text('Provincia: ');    
        $('#div_localidades').hide();    
        $('#div_provincias').show(); 

    } else {
       
        $('#destino').text('Localidades: ')
        $('#div_localidades').show();    
        $('#div_provincias').hide(); 

    }
});


/*
$('#modalTipoResolucion').on('show.bs.modal', function(e) {
    $('#nombre').focus();
});
*/

$('#myModalGanador').on('show.bs.modal', function(e) {

    var empleados = $("#agente").val() + ',' +  ($("#chofer").val()) + ',' + $("#agente_pasaje").val();
    var texto_html = '';
    var ext = $("#externo").is(":checked") ?  2 : 1;

    $("#modal_ganador_texto").html('');

    ( $("#fecha").val().length > 0 ) ? $("#modal_ganador_texto").append('<br><b> Fecha: </b>' + $("#fecha").val() ) : '';
    ( $("#resolucion").val().length > 0 ) ? $("#modal_ganador_texto").append('<br><b> Resolucion: </b>' + $("#resolucion").val() ) : '';
    ( $("#exp4").val().length > 0 ) ? $("#modal_ganador_texto").append('<br><b>Act/exp : </b>' +  $("#exp1").val() + '-' + $("#exp2").val() + '-' + $("#exp3").val() + '-' + $("#exp4").val() + '-' + $("#exp5").val() ) : '';
    
    
    
    if($("#externo").is(":checked")){
        $("#modal_ganador_texto").append('<br><b>Fuera de la provincia: </b>' +  $("#provincias option:selected").text() )
    } else {
       ($("#localidades option:selected").text()) ? $("#modal_ganador_texto").append('<br><b>Localidades :</b>' + $("#localidades option:selected").text() ) : '' ;
    }
   
    ( $("#dias").val().length > 0 ) ? $("#modal_ganador_texto").append('<br><b> Dias: </b>' + $("#dias").val() ) : '';
    ( $("#combustible").val()> 0 ) ? $("#modal_ganador_texto").append('<br><b> Combustible($): </b>' + $("#combustible").val() ) : '';   
    ( $("#gastos").val()> 0 ) ? $("#modal_ganador_texto").append('<br><b> Otros gastos($): </b>' + $("#gastos").val() ) : '';   

    if ($("#agente option:selected").text().length >0) {
        $("#modal_ganador_texto").append('<br><b>Agente/s: </b> <br>');
        $.ajax({                        
                type: "GET",                 
                url: "{{ URL::to('comision_confirmacion') }}",                     
                data: {'empleados' : empleados, 'dias' : $("#dias").val(), 'externo' : ext , 'resolucion' : $("#resolucion").val() },
                success: function(array)             
                {                    
                    if(array[0].resolucion_existente == 0){
                        $("#error_resolucion").hide();
                        $("#error_resolucion").html('');
                        $('#btn_modal').prop('disabled', false);
                    } else {
                        $("#error_resolucion").show();
                        $("#error_resolucion").html('<b>ERROR:</b> El numero de resolucion <b>' + $("#resolucion").val() + '</b> ya existe, elija otro para continuar');
                        $('#btn_modal').prop('disabled', true);                        
                    }
                    array.forEach(function(element) {
                    $("#modal_ganador_texto").append(' - ' + element.empleados + '<BR>');
                    });
                }
            });
    }
    
    });

function dateEs(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}


$("#btnOtrasResoluciones").click(function(){

    $.ajax({                        
        type: "GET",
        url: "{{ URL::to('comision_confirmacion') }}",                     
        data: {'empleados' : 1, 'dias' : $("#dias").val(), 'externo' : 1 , 'resolucion' : $("#resolucion").val() },
        success: function(array)    
        {                    
            if(array[0].resolucion_existente == 0){
                $("#error_resolucion_existente").hide();
                $("#error_resolucion_existente").html('');
                $("#form").submit();
            } else {
                $("#error_resolucion_existente").show();
                $("#error_resolucion_existente_texto").html('<b>ERROR:</b> El numero de resolucion <b>' + $("#resolucion").val() + '</b> ya existe, elija otro para continuar');                    
            }
        }
    });
    
});

$('#closeAlert').on('click', function() {
    $("#error_resolucion_existente").hide();  
});


$( "#btnAgente" ).click(function() {

var url = "{{ URL::to('agente') }}";
var ultimo;

$.ajax({
   headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    type: "post",                 
    url: url,                     
     data: {
    'nombre' : $("input[name=agente_nombre]").val(),
    'cuit' : $("input[name=agente_cuit]").val(),
    'situacion_revista_id' : $("input[name=agente_situacion_revista_id]").val(),
    'escalafon_id' : $("input[name=agente_escalafon_id]").val(),
    'categoria_id' : $("input[name=agente_categoria_id]").val(),
    'apartado' : $("input[name=agente_apartado]").val(),
    'agente_cargo_id' : $('#agente_cargo_id').val(),
    'ceic' : $("input[name=agente_ceic]").val(),
    'grupo' : $("input[name=agente_grupo]").val(),
    'numero' : $("input[name=agente_numero]").val(),
    '_token': $('input[name=_token]').val(),
   
},


    success: function(result)             
    {
        console.log(result);
        arreglojson = [];

        result.lista_select.forEach(function(element) { 
            arreglojson.push(element)
        });

        $('#mensaje_alerta_agente').toggle(3000);
        setTimeout(function ()
        {
            $('#modalAgente').modal('toggle');         
        }, 3700);

        ultimo = $("#nombre").val();


        $('#agente').select2({
            data: result.lista_select,
            theme: 'bootstrap4',  
            allowClear: true
        });
        
        $('#chofer').select2({
            data: result.lista_select,
            theme: 'bootstrap4',  
            allowClear: true
        });



//            $('#agente_cargo_id option').eq(result.lista_select.length).prop('selected', true);
//            $('#agente_cargo_id').trigger('change');                
    }
   });
});


</script>

@endsection